<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace LeoMongo;


use MongoDB\BSON\ObjectId;
use MongoDB\Client;
use MongoDB\Collection;
use Selibra\Config\Config;

abstract class MongoDB
{

    /**
     * 连接管理
     * @var Client
     */
    protected Client $client;

    /**
     * @var string
     */
    public string $database;


    /**
     * 集合名称
     * @var string
     */
    public string $collection;


    /**
     * 连接
     */
    private function connect()
    {
        $this->client = new Client(Config::get('mongodb.uri'));
    }


    /**
     * 获取Manager
     * @return Client
     */
    private function getClient()
    {
        if (!isset($this->client)) {
            $this->connect();
        }
        return $this->client;
    }


    /**
     * 插入数据
     * @param $document
     * @param string $collection
     * @param string $database
     * @return ObjectId
     */
    public function insert($document, $collection = '', $database = '')
    {
        $insertOneResult = $this->getCollection($collection, $database)->insertOne($document);
        return $insertOneResult->getInsertedId();
    }


    /**
     * 修改索引数据
     * @param array $query
     * @param array $document
     * @param string $collection
     * @param string $database
     * @return ObjectId
     */
    public function update($query,array $document, $collection = '', $database = '')
    {
        return $this->getCollection($collection, $database)->updateOne($query, $document);
    }


    /**
     * 查找
     * @param $query
     * @param null $collection
     * @param null $database
     * @return array|object|null
     */
    public function find($query, $collection = null, $database = null)
    {
        return $this->getCollection($collection, $database)->findOne($query);
    }


    /**
     * 删除文档
     * @param $query
     * @param false $justOne
     * @param null $writeConcern
     * @param string $collection
     * @param string $database
     * @return \MongoDB\DeleteResult
     */
    public function delete($query, $justOne = false, $writeConcern = null, $collection = '', $database = '')
    {
        return $this->getCollection($collection, $database)->deleteOne($query, [
            'justOne' => $justOne,
            'writeConcern' => $writeConcern
        ]);

    }


    /**
     * 创建索引
     * @param $index
     * @param string $collection
     * @param string $database
     * @return string
     */
    public function createIndex($index, $collection = '', $database = '')
    {
        return $this->getCollection($collection, $database)->createIndex($index);
    }


    /**
     * @param string $collection
     * @param string $database
     * @return Collection
     */
    public function getCollection($collection = '', $database = '')
    {
        $database = $database ?: (isset($this->database) ? $this->database : Config::get('mongodb.database'));
        $collectionName = $collection ?: $this->collection;
        return $this->getClient()->$database->$collectionName;
    }

}